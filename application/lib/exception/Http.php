<?php
namespace app\lib\exception;

use Exception ;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\ValidateException;
use think\Request;

class Http extends Handle
{
    private  $code;
    private  $msg ;
    private  $errorcode;
    public function render(Exception   $e){
        if($e instanceof  BaseException){
            $this->code= $e->code;
            $this->msg = $e->msg;
            $this->errorcode  = $e->errorCode;
        }else{
            if(config('app.app_debug')) {
                return parent::render($e);
            }
            $this->code = 500;
            $this->msg = '服务器内部错误';
            $this->errorcode = '999';
        }
        $res = [
            'msg'=>$this->msg,
            'errorCode'=>$this->errorcode,
        ];
        return json($res,$this->code);
    }

}