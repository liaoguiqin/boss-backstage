<?php


namespace app\common\validate;
use app\lib\exception\BaseException;
use think\Validate;

class BaseValidate extends Validate
{
    public function v_check($scene = '')
    {
        // 获取请求传递过来的所有参数
        $params = request()->param();
        $check  =  empty($scene) ? $this->check($params) : $this->scene($scene)->check($params) ;
        // 开始验证
        if (! $check) {
            throw  new BaseException(['msg' => $this->getError(), 'errorCode' => 10000, 'code' => 400]);
        }
        return true;
    }

}
